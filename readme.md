# URKL Url Shortener  

URKL is a URL shortener built using NodeJS, Express, MongoDB, and React. It supports user accounts and private shortcodes.

## Requirements
* Node v10.0+
* MongoDB v3.4.13+ w/ enforced authentication
* SSL Certificate and Key

## Compatibility
Has been tested with:
* Ubuntu Server 16.04  
* Arch Linux (as of 3/14/18)  
* macOS 10.13


__Works best with Ubuntu Server 16.04 and macOS 10.13__  

## Installation  

### Clone and install project

1. Clone urkl-js to any `directory`  

> git clone https://gitlab.com/etroimcasso/urkl-js.git `directory`

2. Change directory to `directory`  

> cd `directory`  

3. Install dependencies  

> npm install  

4. Install pm2  

> npm install pm2 -g  

### Configure project settings 
The .env file is used to configure your application's main features and database connections.
Below is a boilerplate .env file for URKL.  

>#### __.env__  
>```
>HTTP_PORT=8000  
>HTTPS_PORT=443  
>BASE_URL=mysite.com  
>SHORTCODE_LENGTH=4  
>MONGO_HOST=localhost  
>MONGO_PORT=27017  
>MONGO_DB=urkl  
>MONGO_USER=  
>MONGO_PASS=  
>SESSION_SECRET=  
>LANG_PROC=false  
>NODE_ENV=production  
>NO_ROBOTS=true
>```   
NODE_ENV can be either 'production' or 'dev' (without the quotes)  
NO_ROBOTS prevents web scrapers from accessing the site

The .env file should go in the root directory of your project. URKL will not start without the .env file

### Configure SSL options
The SSL certificate files go in the `certs` folder.  
>key: server.key  
>certificate: server.crt  

#### SSL with Let's Encrypt
The Let's Encrypt tool will generate a number of files, but the important ones are:
>`privkey.pem` - rename to `server.key`  
>`fullchain.pem` - rename to `server.crt`

## Running URKL
Once you've installed the dependencies and configured .env, you will need to build the client application:  
> npm run webpack  

After this has completed, you can run node directly without building, unless the application receives an update:  
  
> npm start

## Misc. Info
The maximum number of shortcodes available is 
62^(shortcode length)
