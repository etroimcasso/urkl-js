//Config.js
//Urkl configuration

module.exports = {
    text: {
        //Whenever the application is referenced in body text
        application_title: "URKL",
        badge_text: 'URKL', //Can contain HTML
        application_info: 'URKL URL Shortener',
        //The base title that will be displayed on the tab. <title> in the head
        page_title: 'URKL',
        //Badge text displayed in the top bar
    },
    topbar: {
        size: 'huge',
        menu_color: 'black', //Use black for customizable badge and item colors
        badge_color: 'blue',
        newLink_color: '',
        profile_color: '',
        signin_button_color: 'blue',
    },
    titlebar_color: '#0E6EB8', //Blue
    //titlebar_color: '#000000', //Black
    bottombar: {
        color: 'black',
        size: 'huge',
        refreshButton_position: 'right',
        editButton_position: 'left'
    },
    label_size: "large",
    tag_size: "tiny",
    private_label_color: "violet",
    views_label_color: "blue",
    create_account_button_color: 'grey',
    share_button: {
        color: 'blue',
        size: 'small',
    },
    newLink_form_size: "small",
    edit_mode_color: "orange",
    link_hover_color: 'grey',
    params: {
        link_desc_max_char_count: 250,
        title_max_char_count: 60,
        trimmed_text_suffix: "...",
        min_username_length: 5,
        max_username_length: 18,
        min_password_length: 10,
        max_password_length: 24
     }, 
     profile_popup_size: "large",
     coppa_display: 'false',
     admin: {
        access_all_private_items: false, //Admin can see / edit all private items
        edit_mode: {
            all_public_items: true, //Can access / edit ALL public items
        }
     }
}
