import React, { Component } from 'react';
import { Popup } from 'semantic-ui-react';
import CopyLinkToClipboard from '../buttons/CopyLinkToClipboard';
import Configuration from '../../../config';

export default class SharePopup extends Component {

    render() {
        const { trigger, shortcode_url, position } = this.props;
        return (
            <Popup 
                flowing
                hideOnScroll
                className="sharePopup"
                trigger={ trigger}
                on='click'
                position={position}
            > 
                <Popup.Content>
                    <CopyLinkToClipboard shortcode_url={ shortcode_url } />
                </Popup.Content>
            </Popup>           
        );
    }
}
