/* 

    + Needs to be able to display user info for ANY user
    + Needs to allow editing if the user being viewed is also the current user

    Props:
        currentUser: the logged-in user
        user: the user whose profile to show
        user_data: the data for the profile - /server/lib/models/User.js for information
*/

import React, { Component } from 'react';
import { Button, Popup } from 'semantic-ui-react';
import SignoutButton from '../buttons/SignoutButton';

export default class UserProfilePopupContent extends Component {
    render () {
        const { onSignout } = this.props;
        return (
            <SignoutButton onSignout={onSignout} />
        )
    }
}
