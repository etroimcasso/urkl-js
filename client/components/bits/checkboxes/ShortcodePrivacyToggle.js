import React, { Component } from 'react';
import { Checkbox, Icon } from 'semantic-ui-react';
import axios from 'axios';


/*
To make this universal:
Props: 
    privacy (BOOLEAN): Initial value
    -handleToggle (function(BOOLEAN)): should send a (toggleValue) to this function that = privacy 
        - the receiving function should take the boolean returned from this to mean is_public (true = public, false = private),
        ---So, the opposite of checked / this.state.privacy
    -updateDatabase (BOOLEAN): determines if this will update database entries or simply return an is_public value
*/
export default class ShortcodePrivacyToggle extends Component {
    constructor(props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
    }

    componentDidMount() {
    }

    handleToggle() {
        var toggleVal = !this.props.privacy;
        if (this.props.updateDatabase) {
            this.props.handleToggle(!toggleVal)
            axios.post('/api/sc/updatePrivacySetting', {
                shortcode: this.props.shortcode
            }).then((response) => {

            }).catch((error) => {
                this.props.handleToggle(toggleVal);
            })
        } else {
            this.props.handleToggle(!toggleVal)
        }
    }

    render() {
        const { privacy } = this.props;
        return (
            <span display='block'>
                <Icon name='globe' id="publicPrivacyIcon" />
                <Checkbox toggle checked={privacy} onChange={this.handleToggle} />
                <Icon name='lock' id="privatePrivacyIcon" />
            </span>
        );

    }
}
