import React, { Component } from 'react';
import { Form, Button, Input, Divider, Label } from 'semantic-ui-react';
import Configuration from '../../../config';
import axios from 'axios';

//Props: handleSubmit: a function to call once the login succeeds
export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            login_error: false,
            createAccount_open: false
        }

        this.handleSubmitClick = this.handleSubmitClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.setErrorState = this.setErrorState.bind(this);
        this.openCreateAccountModal = this.openCreateAccountModal.bind(this);
        this.closeCreateAccountModal = this.closeCreateAccountModal.bind(this);
    }

    setErrorState(errorState) {
        this.setState({
            login_error: errorState
        })
    }

    handleSubmitClick() {
        axios.post('/api/u/login',{
            username: this.state.username,
            password: this.state.password
        }).then((response) => {
            if (response.data.response) {
                this.setErrorState(false);
                this.props.handleSubmit();
            } else {
                this.setErrorState(true);
            }
        }).catch((error) => {
            this.setErrorState(true)
        });
    }

    handleChange = (e, { name, value }) => {
        this.setState({ 
            [name]: value, 
         });
        this.setErrorState(false);
    }

    openCreateAccountModal() {
        this.setState({
            createAccount_open: true
        })
    }

    closeCreateAccountModal() {
        this.setState({
            createAccount_open: false
        })
    }

    render() {
        const { username, password, login_error, createAccount_open } = this.state;
        const { onCreateAccountClick } = this.props;

        return (
            <div>
                <Form>
                    <Form.Field error={login_error}>
                        <label>Username</label>
                        <Input name='username' 
                            placeholder="Username"
                            value={username} 
                            onChange={this.handleChange} 
                            maxLength={Configuration.params.max_username_length}
                        />
                    </Form.Field>
                    <Form.Field error={login_error}>
                        <label>Password</label>
                        <Input name="password" 
                            placeholder="Password" 
                            type='password' 
                            value={password} 
                            onChange={this.handleChange} 
                        />
                    </Form.Field>
                    <Form.Button fluid content="Sign in" color={Configuration.topbar.signin_button_color} onClick={this.handleSubmitClick} />
                    <Divider id="logInDivider" fitted />
                </Form>
                <Label size="large" id="createAccountOrLabel" circular>or</Label>
                <Button fluid id="createAccountButton" color={Configuration.create_account_button_color} onClick={onCreateAccountClick}> Create Account </Button>
            </div>
        );
    }

}