import React, { Component } from 'react';
import { Form, Input, Checkbox, Button, Divider, Label } from 'semantic-ui-react';
import Configuration from '../../../config';
import axios from 'axios';


/*
    *When any input receives an error, set state.error.[name] to true
    *When input receives input change (onChange), set error to false
    *Whenever password_{1,2} are modified, run this.passwordCheck()

    Props:
    onSuccess:
    onCancelClick:

*/
export default class CreateAccountForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data_username: "",
            data_email: "",
            data_password_1: "",
            data_password_2: "",
            data_coppa_agree: false,
            error_username: true,
            error_email: true,
            error_password_1: true,
            error_password_2: true,
            passwords_equal: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitClick = this.handleSubmitClick.bind(this);
        this.handleCoppaToggle = this.handleCoppaToggle.bind(this);
        this.validateFormInput = this.validateFormInput.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
    }

    validateFormInput(name, value) {
        var { data_username, data_email, data_password_1, data_password_2} = this.state;
        var error_value;
        switch (name) {
        case 'username':
            error_value = ( value.length < Configuration.params.min_username_length
                            || value.length > Configuration.params.max_username_length );
            break;
        case 'email':
            error_value = ( value.length == 0) || !this.validateEmail(value);
            break;
        case 'password_1':
            error_value = (data_password_2 != value && data_password_2.length != 0) && value.length < Configuration.params.min_password_length;
            this.setState({
                error_password_2: error_value || data_password_2 < Configuration.params.min_password_length 
            })
            break;
        case 'password_2':
            error_value = (data_password_1 != value && data_password_1.length != 0) && value.length < Configuration.params.min_password_length;;
            this.setState({
                error_password_1: error_value || data_password_1 < Configuration.params.min_password_length  
            })
            break;
        }
        return error_value;
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
    }

    handleChange = (e, { name, value }) => {
        var data_id = "data_" + name;
        var error_id = "error_" + name;
        var error_value = this.validateFormInput(name, value);

        this.setState({ 
            [data_id]: value,
            [error_id]: error_value 
         });

    }

    handleCoppaToggle() {

    }

    handleSubmitClick(){
        axios.post('/api/u/new',{ 
            username: this.state.data_username,
            email: this.state.data_email,
            password: this.state.data_password_1
        }).then((response) => {
            if (response)  {
                axios.post('/api/u/login',{
                    username: this.state.data_username,
                    password: this.state.data_password_1
                }).then((response) => {
                    console("response.data.error: " + response.data.error)
                    if (!response.data.response) {
                        this.props.onCreateAccount(true);
                    } else {
                        console("response.data.error: " + response.data.error);
                        if (response.data.error === "username") {
                            this.setState({
                                error_username: true
                            })
                        }
                        this.props.onCreateAccount(false);
                    }
                }).catch((error) => {
                    this.props.onCreateAccount(false);
                });
            }
        }).catch((error) => {
            this.props.onCreateAccount(false);
        })
    }

    handleAddConfirm() {
        this.props.onCreateAccount(true);
    }

    render() {
        const { data_username, data_email, passwords_equal,
                data_password_1, data_password_2, 
                data_passwords_equal, data_coppa_agree,
                error_username, error_email, error_password_1, error_password_2
            } = this.state;
        const { open, onCreateAccountCancelClick } = this.props;

        const submitButtonDisabled = (error_username || error_email || error_password_1 || error_password_2)
            && (Configuration.coppa_display && !data_coppa_agree);


        return (
            <div>
                <Form>
                    <Form.Input 
                        label="Username"
                        name="username"
                        placeholder="Username"
                        type="text"
                        id="createUserUsernameInput"
                        onChange={this.handleChange}
                        value={data_username}
                        maxLength={Configuration.params.max_username_length}
                        error={error_username}
                    />
                    <Form.Input
                        label="Email"
                        name="email"
                        placeholder="Email"
                        type="text"
                        onChange={this.handleChange}
                        value={data_email}
                        error={error_email}
                    />
                     <Form.Input
                        label="Password"
                        name="password_1"
                        placeholder="Password"
                        type="password"
                        onChange={this.handleChange}
                        value={data_password_1}
                        maxLength={Configuration.params.max_password_length}
                        error={error_password_1}
                    />
                     <Form.Input
                        label="Confirm Password"
                        name="password_2"
                        placeholder="Confirm Password"
                        type="password"
                        onChange={this.handleChange}
                        value={data_password_2}
                        error={error_password_2}
                    />
                    { Configuration.coppa_display == true &&
                        <Checkbox checked={data_coppa_agree} onChange={this.handleCoppaToggle} />
                    }
                    <Form.Button 
                            disabled={submitButtonDisabled}
                            fluid 
                            content="Create Account" 
                            color={Configuration.topbar.signin_button_color} 
                            onClick={this.handleSubmitClick} />
                    <Divider id="createAccountDivider" fitted />
                </Form>
                <Label size="mini" id="createAccountFormOrLabel" >or</Label>
                <Button fluid id="createAccountCancelButton" color="red"  onClick={onCreateAccountCancelClick}>Cancel</Button>
            </div>

        );
    }
}