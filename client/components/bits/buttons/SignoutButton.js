import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';
import axios from 'axios';

export default class SignoutButton extends Component {
    constructor(props) {
        super(props);

        this.handleSignout = this.handleSignout.bind(this);
    }

    handleSignout() {
        axios.get('/api/u/logout')
            .then((response) => {
                if (response) 
                    this.props.onSignout();
            })
            .catch((error) => {
            });
    }
    render () {
        return (
            <Button fluid content="Sign out" onClick={this.handleSignout} />
        )
    }
}
