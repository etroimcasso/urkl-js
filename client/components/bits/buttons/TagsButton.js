import React, { Component } from 'react';
import { Popup, Label, Icon } from 'semantic-ui-react';
import ShortcodeTags from '../labels/ShortcodeTags';
import Configuration from '../../../config';

export default class TagsButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { tags, position, onTagClick, shortcode } = this.props;
        const tagsCount = (tags.length === 1 && tags[0] === "" ) ? 0 : tags.length;

        const triggerButton = (
            <Label className="tagsPopupTriggerLabel"> 
                <Icon name='tags' />
                { tagsCount }
            </Label>
        );

        return (
            <span>
                { tagsCount > 0 &&
                <Popup 
                    flowing
                    hideOnScroll
                    className="sharePopup"
                    trigger={ triggerButton }
                    on={['click']}
                    position='top center' > 
                    <Popup.Content>
                        <ShortcodeTags onClick={onTagClick} tags={ tags } shortcode={shortcode} />
                    </Popup.Content>
                </Popup>
                }
                { tagsCount === 0 &&
                    <Label> 
                        <Icon name='tags' />
                        { tagsCount }
                    </Label>
                }
            </span>       
        );
    }
}
