import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import SharePopup from '../popups/SharePopup';
import Configuration from '../../../config';

export default class ShareButton extends Component {

    render() {
        const { shortcode_url, floated } = this.props;
        return (
            <SharePopup position="top right" shortcode_url={shortcode_url} trigger={ 
                <Button icon
                    size={Configuration.share_button.size}
                    floated={floated}
                    color={Configuration.share_button.color} >
                    <Icon name='share' />
            </Button> } />
        )
    }

}
