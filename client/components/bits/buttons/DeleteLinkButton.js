import React, { Component } from 'react';
import { Button, Icon, Popup } from 'semantic-ui-react';

/*
Props: 
    item_key: key for item
    floated: Floating position of button. Can be null
    linksDeleteLoading: Determines if the icon is a loading icon or not
    color: button color
*/
export default class DeleteLinkButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            popup_open: false
        }
        this.handleDeleteConfirmClick = this.handleDeleteConfirmClick.bind(this);
        this.handleDeleteButtonClick = this.handleDeleteButtonClick.bind(this);
        this.closePopup = this.closePopup.bind(this);
    }
    
    handleDeleteConfirmClick() {
        this.closePopup();
        this.props.onDeleteClick(this.props.item_key);
    }


    closePopup() {
        this.setState({
            popup_open: false
        })    
        //Run a function from the parent container that tells the other delete buttons they can enable again
        this.props.handleDeletePopupOpen(false);
    }

    handleDeleteButtonClick() {
        this.setState({
            popup_open: true
        })
        //Run a function from the parent container that tells the other delete buttons to disable for now
        this.props.handleDeletePopupOpen(true);
    }

    render() {
        const { floated, linksDeleteLoading, linkDeleting, item_key, anyDeletePopupOpen } = this.props;

        //const color = this.props.color;
        const color = 'red';

        const {popup_open } = this.state;
        const deleteButton = (linksDeleteLoading && item_key == linkDeleting) ? 
        (<Button icon
            floated={floated}
            color={color} 
            disabled={true}>
            <Icon loading={linksDeleteLoading} name='trash' />
        </Button>) :( 
            ( linksDeleteLoading ) ?
                (<Button icon
                    floated={floated}
                    color={color} 
                    disabled={true}>
                    <Icon name='trash' />
                </Button>): (anyDeletePopupOpen) ? 
                    (<Button icon
                        floated={floated}
                        color={color}
                        disabled={true} >
                        <Icon name='trash' />
                    </Button>
                    )
                    :
                    (<Button icon
                        floated={floated}
                        color={color}
                        onClick={this.handleDeleteButtonClick}
                        >
                        <Icon name='trash' />
                    </Button>
                    )
                );


        return (
            <Popup on='click'
                open={ popup_open }
                trigger={ deleteButton }
            >
                <Popup.Header>
                Are you sure?
                </Popup.Header>
                <Popup.Content>
                    <Button.Group>
                        <Button color={color} onClick={this.handleDeleteConfirmClick} >Yes</Button>
                        <Button onClick={this.closePopup}>No</Button>
                    </Button.Group>
                </Popup.Content>
            </Popup>
        )
    }
}
