import React, { Component } from 'react';
import { Input, Label, Icon } from 'semantic-ui-react';
import Configuration from '../../../config';


export default class CopyLinkToClipboard extends Component {
    constructor(props) {
        super(props);

        this.handleCopyButtonClick = this.handleCopyButtonClick.bind(this);
    }

    handleCopyButtonClick() {
        console.log("Copy to clipboard");
        var text = this.props.shortcode_url;
        text.select();
        document.execCommand("Copy");
    }

    render() {
        const { shortcode_url } = this.props;
        const color = Configuration.share_button.color

        const confirmCopy_button = ( true == true ) ? { color: color, labelPosition: 'right', icon: 'copy', content: "Copy" } : null;
        const copyLabel = (
            <Label className="clickableLabel copyToClipboardLabel" icon='copy' color={ color } onClick={this.handleCopyButtonClick} />
        );

        return (
            <Input 
                fluid
                readOnly
                label={ copyLabel }
                onClick={this.handleCopyButtonClick}
                size='large'
                defaultValue={ shortcode_url }
            />
        );
    }
}
