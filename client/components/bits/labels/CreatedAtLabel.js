import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
import moment from 'moment';
import moment_tz from 'moment-timezone'

export default class CreatedAtLabel extends Component {
    render() {
        return (
            <span className='date'>
                <Icon name='time' />{moment(moment(this.props.createdAt).tz(moment.tz.guess())).calendar() + " " + moment.tz(this.props.createdAt,moment.tz.guess()).zoneAbbr()}
            </span>            
        );
    }

}
