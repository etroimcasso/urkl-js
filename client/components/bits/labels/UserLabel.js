import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';

export default class UserLabel extends Component {
    render() {
        const { user } = this.props;
        const labelText = (user == 0) ? "Nobody" : user;
        const labelIcon = (user == 0) ? 'circle outline' : 'user circle';
        return (
            <span>
                <Icon name={labelIcon}/>{labelText}
            </span>
        );
    }

}
