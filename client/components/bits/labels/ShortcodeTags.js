import React, { Component } from 'react';
import { Label, Divider } from 'semantic-ui-react';

class ShortcodeTag extends Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        this.props.onClick(this.props.index, false);
    }

    render() {
        const { index, tagText } = this.props;
        return (
            <Label className="tagLabel" onClick={this.handleClick} size="mini" tag> 
                { tagText }
            </Label>
        );
    }



}

export default class ShortcodeTags extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        const {tags, shortcode, onClick } = this.props;

        return (
            <div>
                { tags.map(function(tag, index) {
                    if (tag != '') {
                        return (
                            <ShortcodeTag className="tagLabel" onClick={onClick} tagText={tag} key={index} index={ index } size="small" tag />
                        )
                    }
                })}
                <Divider className="fullWidthDivider" fitted hidden />
            </div>
        )
    }
}
