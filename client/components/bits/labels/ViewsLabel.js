import React, { Component } from 'react';
import { Icon, Label } from 'semantic-ui-react';

export default class ViewsLabel extends Component {
    render() {
        return (
                <Label>
                    <Icon className="labelIcon" name='unhide'  />
                        {this.props.views}
                </Label>
        );
    }

}