import React, { Component } from 'react';
import { Label, Icon } from 'semantic-ui-react';
import Configuration from '../../../config';

export default class PrivateShortcodeLabel extends Component {
    render() {
        const { shortcode } = this.props;
        return (
            <Label className="cornerPrivacyLabel" key={shortcode} corner='right' size='small' color={ Configuration.private_label_color }>
                <Icon name='lock'/>
            </Label>
        );

    }

}
