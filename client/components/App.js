import React, { Component } from 'react';
import Configuration from '../config';
import TopBar from './TopBar';
import LinkList from './LinkList/LinkList';
import NewLinkModal from './NewLinkModal';
import axios from 'axios';


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logged_in: false,
            user: 0,
            user_data: [],
            userIsAdmin: false,
            newLink_modal_open: false,
            links_data: [],
            loading: true,
            link_delete_loading: false,
            link_deleting: "",
            links_edit_mode: false,
            createAccount_open: false,
            tag_filter: false,        //false is no tag, tag_value otherwise
            user_filter: false,       //false if no user, user_value otherwise
            sortBy: 'dateDescend', //All sorting that is not by tag and username
            sorted_links_data: [],
            base_url: "",
        };
        this.handleContextRef = this.handleContextRef.bind(this);
        this.handleNewLinkModalCancelClick = this.handleNewLinkModalCancelClick.bind(this);
        this.handleNewLinkMenuButtonClick = this.handleNewLinkMenuButtonClick.bind(this);
        this.handleNewLinkSubmissionConfirm = this.handleNewLinkSubmissionConfirm.bind(this);
        this.handleAboutModalClick = this.handleAboutModalClick.bind(this);
        this.handleTopBarBadgeClick = this.handleTopBarBadgeClick.bind(this);
        this.handleEditModeClick = this.handleEditModeClick.bind(this);
        this.getShortcodes = this.getShortcodes.bind(this);
        this.handleShortcodeDelete = this.handleShortcodeDelete.bind(this);
        this.getLoggedInUser = this.getLoggedInUser.bind(this);
        this.setUser = this.setUser.bind(this);
        this.setLoading = this.setLoading.bind(this);
        this.setUserAdmin = this.setUserAdmin.bind(this);
        this.handleLinkListItemPrivacyToggle = this.handleLinkListItemPrivacyToggle.bind(this);
        this.handleLinkListItemViewsChange = this.handleLinkListItemViewsChange.bind(this);
        this.handleTagClick = this.handleTagClick.bind(this);
        this.handleLinkListSortChange = this.handleLinkListSortChange.bind(this);
        this.handleLinkListFilterChange = this.handleLinkListFilterChange.bind(this);
        this.applyLinkListSort = this.applyLinkListSort.bind(this);
        this.applyLinkListFilter = this.applyLinkListFilter.bind(this);
        this.getBaseUrl = this.getBaseUrl.bind(this);
        this.handleSignOut = this.handleSignOut.bind(this);
    }

    componentDidMount() {
        this.getShortcodes();
        this.getLoggedInUser();
        this.getBaseUrl();
    }

    getBaseUrl() {
        const url = window.location;
        var base_url = url.protocol + "//" + url.host + "/";
        this.setState({
            base_url: base_url
        })
    }

    setLoading(loading) {
        this.setState({
            loading: loading,
        });
    }

    setUserAdmin(is_admin) {
        this.setState({
            userIsAdmin: is_admin
        })
    }

    handleEditModeClick() {
        this.setState({
            links_edit_mode: !this.state.links_edit_mode
        })
    }

    handleShortcodeDelete(index) {
        this.setState({
            link_delete_loading: true,
            link_deleting: index
        });
        //Now, remove the object at index from the list_data array, and send an api/sc/rem request with links_data[index].shortcode

        axios.post('/api/sc/rem',{
            shortcode: this.state.links_data[index].shortcode
        }).then((response) => {
            if (response.data.response) {
                var temp_array = this.state.links_data;
                if (index > -1) {
                    temp_array.splice(index, 1);
                    this.setState({
                        links_data: temp_array
                    })
                }

                this.setState({
                    link_delete_loading: false,
                    link_deleting: ""
                })
            }
        }).catch((error) => {
            console.log('error:' + error);
        });
    }

     handleLinkListItemPrivacyToggle(index, is_public) {
        var links_data = this.state.links_data;
        if (index > -1) {
            links_data[index].is_public = is_public;
            this.setState({
                links_data: this.applyLinkListSort(this.state.links_data, this.state.sortBy)
            })
        }
    }

    handleLinkListItemViewsChange(index, views) {
        var links_data = this.state.links_data;
        if (index > -1) {
            links_data[index].views = views;
            this.setState({
                links_data: this.applyLinkListSort(this.state.links_data, this.state.sortBy)
            })
        }
    }

    handleLinkListSortChange(sortBy) {
        this.setState({
            sortBy: sortBy,
            links_data: this.applyLinkListSort(this.state.links_data, sortBy)
        })
    }

    applyLinkListSort(input, sortBy) {
        const setLoading = this.setLoading;

        switch (sortBy) {
            case "dateAscend":
                return input.sort(function(a,b) {
                        return (new Date(a.created_at) - new Date(b.created_at));
                });
            case "dateDescend":
                return input.sort(function(a,b) {
                       return (new Date(b.created_at) - new Date(a.created_at));
                });
            case "titleAscend":
                return input.sort(function(a,b) {
                    var name_a = a.page_title.toLowerCase();
                    var name_b = b.page_title.toLowerCase();
                    if (name_a < name_b)   
                        return -1
                    if (name_a > name_b)
                        return 1
                    return 0;
                });
                break;
            case "titleDescend":
                return input.sort(function(a,b) {
                    var name_a = a.page_title.toLowerCase();
                    var name_b = b.page_title.toLowerCase();
                    if (name_b < name_a)   
                        return -1
                    if (name_b > name_a)
                        return 1
                    return 0;
                });
                break;
            case "userAscend":
                return input.sort(function(a,b) {
                    var user_a = a.user.toLowerCase();
                    var user_b = b.user.toLowerCase();
                    if (user_a < user_b)   
                        return -1
                    if (user_a > user_b)
                        return 1
                    return 0;
                });
                break;
            case "userDescend":
                return input.sort(function(a,b) {
                    var user_a = a.user.toLowerCase();
                    var user_b = b.user.toLowerCase();
                    if (user_b < user_a)   
                        return -1
                    if (user_b > user_a)
                        return 1
                    return 0;
                });
                break;
            case "urlAscend":
                break;
            case "urlDescend":
                break;
            case "viewsAscend":
                return input.sort(function(a,b) {
                       return a.views-b.views;
                });
                break;
            case "viewsDescend":
                return input.sort(function(a,b) {
                       return b.views-a.views;
                });
                break;
        }

    }

    handleLinkListFilterChange() {
        
    }


    applyLinkListFilter(input) {
        if (this.state.user_filter) {

        }

        if (this.state.tag_filter) {

        }

        return input;
    }

    handleTagClick(link_index, tag_index, do_delete) {
        var tag = this.state.links_data[link_index].tags.split(",")[tag_index];
        this.setState({
            tag_sort: tag
        })
        console.log(tag);
    }

    getLoggedInUser() {
        this.setLoading(true);
        axios.get('/api/u/getSessionUserData')
        .then((response) => {
            if (response.data.response == 0) {
                this.setUser(0);
                //this.setLoading(false);
                //this.getShortcodes();
                this.setUserAdmin(false);
            } else {
                this.setUser(response.data.response);
               // this.setLoading(false);
                //this.getShortcodes();
                this.setUserAdmin(response.data.response.admin);
            }
        })
        .catch((error) =>{
                this.setUser(0);
                //this.setLoading(false);
                //this.getShortcodes();
                this.setUserAdmin(false);
        })

    }

    handleSignOut() {
        this.setState({
            user: 0,
            userIsAdmin: false
        })
    }

    setUser(user) {
        this.setState({
            user: (user != 0) ? user.username : 0,
            user_data: (user != 0) ? user : [],
            link_delete_loading: false,
            link_deleting: "",
            links_edit_mode: false,
            loading: false
        })
    }

    handleContextRef = contextRef => this.setState({ contextRef })

    handleNewLinkModalCancelClick() {
        this.setState({
            newLink_modal_open: false
        });
    }

    handleNewLinkMenuButtonClick() {
        this.setState({
            newLink_modal_open: true
        });
    }

    handleNewLinkSubmissionConfirm() {
        this.setState({
            newLink_modal_open: false
        });
        this.getShortcodes();
    }

    getShortcodes() {
        if (!this.state.loading)
            this.setLoading(true);
        axios.get('/api/sc/lists/all')
            .then((response) => {
                this.setState({
                    links_data: this.applyLinkListSort(response.data.response, this.state.sortBy),
                 })
                this.setLoading(false);
            })
            .catch((error) => {
                this.setState({
                    links_data: []
                })
                this.setLoading(false);
            });
    }

    handleAboutModalClick() {
        this.setState({
            about_modal_open: false
        })
    }

    handleTopBarBadgeClick() {
        this.setState({
            about_modal_open: true
        })
    }


    render() {
        const { contextRef, newLink_modal_open, user, user_data, loading, 
            about_modal_open, links_edit_mode, link_delete_loading, link_deleting, userIsAdmin, createAccount_open, sortBy, links_data, base_url} = this.state;


        //const links_data = this.applyLinkListSort(this.state.links_data);

        return (
            <div className="App" ref={this.handleContextRef}>
                <NewLinkModal
                    user={user}
                    open={ newLink_modal_open } 
                    onClickCancel={this.handleNewLinkModalCancelClick} 
                    onSubmitConfirm={this.handleNewLinkSubmissionConfirm}
                    baseUrl={base_url}
                    />
                <TopBar 
                    attached='top'
                    context={contextRef} 
                    user={user} 
                    userIsAdmin={userIsAdmin}
                    onClickNewLinkButton={this.handleNewLinkMenuButtonClick} 
                    linksEditMode={links_edit_mode}
                    onLoginFormSubmit={this.getLoggedInUser}
                    onSignout={this.handleSignOut}
                />
                <LinkList  
                    attached='bottom'
                    onRefreshClick={this.getShortcodes} 
                    onDeleteClick={this.handleShortcodeDelete}
                    setLoading={this.setLoading}
                    linksLoading={loading}
                    linksDeleteLoading={link_delete_loading}
                    linkDeleting={link_deleting}
                    linksData={links_data} 
                    user={user}
                    userIsAdmin={userIsAdmin}
                    contextRef={contextRef} 
                    editModeClick={this.handleEditModeClick}
                    editMode={links_edit_mode}
                    handlePrivacyToggle={this.handleLinkListItemPrivacyToggle}
                    handleViewsChange={this.handleLinkListItemViewsChange}
                    onTagClick={this.handleTagClick}
                    sortBy={sortBy}
                    changeSort={this.handleLinkListSortChange}
                    baseUrl={base_url}
                    />
            </div>
        );
    }
}
