import React, { Component } from 'react';
import { Menu, Icon, Popup, Header, Divider, Transition }  from 'semantic-ui-react';


/* Props:
    sortType={sortBy}
    changeSort=this.handleSortChange(sortby)
*/
export default class SortMenu extends Component {
    constructor(props) {
        super(props);

        this.changeSorting = this.changeSorting.bind(this);
    }

    changeSorting(e, {name }) { 
        this.props.changeSort(name);
    }

    render() {
        const { sortType, trigger, open} = this.props;

        const dateIcon = <Icon name='time' />;
        const viewsIcon = <Icon name='unhide' />;
        const titleIcon = <Icon name='book' />;
        const userIcon = <Icon name='user circle' />;
        const popupPosition = "top center";
        const popupVerticalOffsetTopRow = 9; 
        const popupVerticalOffsetBottomRow = 49;
        
        var sortText = {
            dateAscend: "Oldest",
            dateDescend: "Newest",
            titleAscend: "Title A - Z",
            titleDescend: "Title Z - A",
            userAscend: "Username A - Z",
            userDescend: "Username Z - A",
            viewsAscend: "Least viewed",
            viewsDescend: "Most viewed",
        }

        const popupHeaderText = sortText[sortType];

        const dateDescendIcons = (
            <span>
                { dateIcon }
                <Icon name='sort numeric descending' />
            </span>
            );
        const dateAscendIcons = (
            <span>
                { dateIcon }
                <Icon name='sort numeric ascending' />
            </span>
            );

        const viewsDescendIcons = (
            <span>
                { viewsIcon }
                <Icon name='sort numeric descending' />
            </span>
            );

        const viewsAscendIcons = (
            <span>
                { viewsIcon }
                <Icon name='sort numeric ascending' />
            </span>
            );

        const titleAscendIcons = (
            <span>
                { titleIcon }
                <Icon name='sort alphabet ascending' />
            </span>
            );
        const titleDescendIcons = (
            <span>
                { titleIcon }
                <Icon name='sort alphabet descending' />
            </span>
            );

        const userAscendIcons = (
            <span>
                { userIcon }
                <Icon name='sort alphabet ascending' />
            </span>
            );

        const userDescendIcons = (
            <span>
                { userIcon }
                <Icon name='sort alphabet descending' />
            </span>
            );

        const dateSortDescendButton = (sortType == 'dateDescend')  ? 
            (   
                <Menu.Item name='dateDescend' active={true} children={dateDescendIcons} />
            ):
            ( 
                <Menu.Item name='dateDescend' active={false} onClick={this.changeSorting} children={dateDescendIcons} />
            );

        const dateSortAscendButton = (sortType === 'dateAscend') ? 
            (   
                <Menu.Item name='dateAscend' active={true} children={dateAscendIcons} />
            ):
            (
                <Menu.Item name='dateAscend' active={false} children={dateAscendIcons} onClick={this.changeSorting} />
            );

        const viewsSortAscendButton = (sortType === 'viewsAscend') ? 
            (   
                <Menu.Item name='viewsAscend' active={true} children={viewsAscendIcons}/>
            ):
            (
                <Menu.Item name='viewsAscend' active={false} onClick={this.changeSorting} children={viewsAscendIcons} />

            );  

        const viewsSortDescendButton = (sortType === 'viewsDescend') ? 
            (   
                <Menu.Item name='viewsDescend' active={true} children={viewsDescendIcons}/>
            ):
            (
                <Menu.Item name='viewsDescend' active={false} onClick={this.changeSorting} children={viewsDescendIcons} />

            );

        const titleSortAscendButton = (sortType === 'titleAscend') ? 
            (   
                <Menu.Item name='titleAscend' active={true} children={titleAscendIcons}/>
            ):
            (
                <Menu.Item name='titleAscend' active={false} onClick={this.changeSorting} children={titleAscendIcons} />

            );

        const titleSortDescendButton = (sortType === 'titleDescend') ? 
            (   
                <Menu.Item name='titleDescend' active={true} children={titleDescendIcons}/>
            ):
            (
                <Menu.Item name='titleDescend' active={false} onClick={this.changeSorting} children={titleDescendIcons} />

            );

        const nameSortAscendButton = (sortType === 'userAscend') ? 
            (   
                <Menu.Item name='userAscend' active={true} children={userAscendIcons}/>
            ):
            (
                <Menu.Item name='userAscend' active={false} onClick={this.changeSorting} children={userAscendIcons} />

            );  

        const nameSortDescendButton = (sortType === 'userDescend') ? 
            (   
                <Menu.Item name='userDescend' active={true} children={userDescendIcons}/>
            ):
            (
                <Menu.Item name='userDescend' active={false} onClick={this.changeSorting} children={userDescendIcons} />

            );  
        
        const popupHeader = <Header textAlign='center' >{popupHeaderText}</Header>;

        return (
            <div>
            <Transition visible={open} animation="fade" duration={{show: 200, hide: 200}} unmountOnHide > 
                <Popup id="linksSortBar" inverted flowing basic open>
                    <Menu className="linksSortMenu" inverted borderless  compact>
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetTopRow} trigger={ dateSortDescendButton } content='Newest first' />
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetTopRow} trigger={ viewsSortDescendButton } content='Most views' />
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetTopRow} trigger={ titleSortAscendButton } content="Title A-Z" />
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetTopRow} trigger={ nameSortAscendButton } content="Username A-Z" />   
                    </Menu>
                    <br/>
                    <Menu className="linksSortMenu" inverted borderless compact>
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetBottomRow} trigger={ dateSortAscendButton } content='Oldest first' />
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetBottomRow} trigger={ viewsSortAscendButton } content='Least views' />
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetBottomRow} trigger={ titleSortDescendButton } content="Title Z-A" /> 
                        <Popup hideOnScroll position={popupPosition} verticalOffset={popupVerticalOffsetBottomRow} trigger={ nameSortDescendButton } content="Username Z-A" /> 

                    </Menu>
                </Popup>
            </Transition>
            </div>
        );

    }
}