import React, { Component } from 'react';
import { Container, Button, Icon, Menu, Image, Card, Loader, Dimmer, Step, Divider, Popup } from 'semantic-ui-react';
import SortMenu from './SortMenu';
import LinkListItem from './LinkListItem';
import Configuration from '../../config';


export default class LinkList extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            anyDeletePopupOpen: false,
            sortBar_open: false,
        };

        this.handleEditModeClick = this.handleEditModeClick.bind(this);
        this.handleDeletePopupOpen = this.handleDeletePopupOpen.bind(this);
        this.handlePrivacyToggle = this.handlePrivacyToggle.bind(this);
        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.handleSortBarToggle = this.handleSortBarToggle.bind(this);
    }

    //In the future, this listing will allow search / filter parameters
    //For now (for testing purposes) this will simply retrieve all 


    
    handleDeletePopupOpen(open) {
        this.setState({
            anyDeletePopupOpen: open
        })
    }

    handleEditModeClick() {
        this.setState({
            anyDeletePopupOpen: false
        })
        this.props.editModeClick();
    }

    handlePrivacyToggle(index, is_public) {
        this.props.handlePrivacyToggle(index, is_public);
    }

    handleChangeSort(name) {
/*        this.setState({
            sortBar_open: false
        })*/
        this.props.changeSort(name);
    }

    handleSortBarToggle() {
        this.setState({
            sortBar_open: !this.state.sortBar_open
        })
    }

    render() {
        const { contextRef, sortBar_open } = this.state;
        const { 
            onRefreshClick, linksData, linksLoading, user, style, onDeleteClick, 
            linksDeleteLoading, linkDeleting, editMode, userIsAdmin, handleViewsChange, onTagClick, setLoading,  sortBy, baseUrl } = this.props;
        const { anyDeletePopupOpen } = this.state;
        const handleAnyDeletePopupsOpen = this.handleDeletePopupOpen;
        const handlePrivacyToggle = this.handlePrivacyToggle;


        //Always false option for now
        const bottombar_color = ( editMode != editMode ) ? Configuration.edit_mode_color : Configuration.bottombar.color;

        const editButton = (linksDeleteLoading) ? 
        (            
            <Menu.Item position="left" name="editMode" color={Configuration.edit_mode_color} active={editMode} >
                <Icon loading name='hourglass outline' />
            </Menu.Item>)
        : (user != 0) ? (
            <Menu.Item position="left" name="editMode" color={Configuration.edit_mode_color} active={editMode} onClick={this.handleEditModeClick}>
                <Icon name='pencil' />
            </Menu.Item>) :
        (
            <Menu.Item position="left" name="editMode" color={Configuration.edit_mode_color} disabled={true} onClick={this.handleEditModeClick}>
                <Icon name='pencil' />
            </Menu.Item>
        );

        const refreshButton_position = Configuration.bottombar.refresh_position;
        const editButton_position = Configuration.bottombar.edit_position;

        const dateIcon = <Icon name='time' />;
        const viewsIcon = <Icon name='unhide' />;
        const titleIcon = <Icon name='book' />;
        const userIcon = <Icon name='user circle' />;

        var filterIcon = "";
        switch (sortBy) {
            case "dateAscend":
                filterIcon = ( 
                    <span>
                        { dateIcon }
                        <Icon name='sort numeric ascending' />
                    </span>
                );
                break;
            case "dateDescend":
                filterIcon = ( 
                    <span>
                        { dateIcon }
                        <Icon name='sort numeric descending' />
                    </span>
                );
              break;
            case "titleAscend":
                filterIcon = ( 
                    <span>
                        { titleIcon }
                        <Icon name='sort alphabet ascending' />
                    </span>
                );
                break;
            case "titleDescend":
                filterIcon = ( 
                    <span>
                        { titleIcon }
                        <Icon name='sort alphabet descending' />
                    </span>
                );
               break;
            case "userAscend":
                filterIcon = (
                    <span>
                        { userIcon }
                        <Icon name='sort alphabet ascending' />
                    </span>
                );
               break;
            case "userDescend":
                filterIcon = (
                    <span>
                        { userIcon }
                        <Icon name='sort alphabet descending' />
                    </span>
                );
                break;
            case "urlAscend":
                break;
            case "urlDescend":
                break;
            case "viewsAscend":
                filterIcon = (
                    <span>
                        { viewsIcon }
                        <Icon name='sort numeric ascending' />
                    </span>
                );
               break;
            case "viewsDescend":
                filterIcon = (
                    <span>
                        { viewsIcon }
                        <Icon name='sort numeric descending' />
                    </span>
                );
                break;
        }

        const refreshButton = (editMode) ? 
            (
                <Menu.Item position={refreshButton_position} disabled name='no_refresh' >
                    <Icon.Group>
                        <Icon name='dont' />
                    </Icon.Group>
                </Menu.Item>
            )
            :(linksLoading) ? 
                (
                <Menu.Item position={refreshButton_position} disabled  name='no_refresh' >
                    <Icon loading name='refresh' />
                </Menu.Item>
                )
                :(
                    <Menu.Item  position={refreshButton_position} name='refresh' onClick={onRefreshClick}  >
                        <Icon name='refresh' />
                    </Menu.Item>
                );

        /* The way these filters are to work is as follows:
            An array will store the filter criteria, allowing for multiple filtering criteria.
            Iterate through this array. A switch statement will handle the logic for the different criteria.
            The function will return true only if all criteria are matched. 
            Otherwise, it will return false. Switch statements should only return false, if true then break.
            Return true statement is after Switch statement, only executed if the switch statement does not return any false 

         */
        const filterCriteriaResult = true

        return (
            <Container fluid>
                    <Container fluid className="linkListContainer" attached="bottom" >
                        <Dimmer page active={linksLoading} >
                            <Loader indeterminate size="massive" active={linksLoading} />
                        </Dimmer>
                        <Container>
                            <Divider fitted hidden />
                            <Divider fitted hidden />
                            <Card.Group centered attached='bottom' >
                                {
                                    linksData.map(function (item, index) {
                                        if (item) {
                                            var editModeAdminDisplay = userIsAdmin && ((item.user == 0)  //Default admin behavior
                                                || (item.is_public && Configuration.admin.edit_mode.all_public_items)); 
                                                //^^^^Will allow edit of ALL public items if Configuration.admin.edit_mode.all_public_items is true
                                            if (                                        
                                                //Will allow admin access to ALL private items if Configuration.admin.edit_mode.all_private_items is true                   
                                                (!editMode && ( item.is_public == false && user == item.user) || Configuration.admin.access_all_private_items ) 
                                                || (!editMode && item.is_public == true) 
                                                || (editMode && (user == item.user) || editModeAdminDisplay)
                                                && filterCriteriaResult )  {
                                                return (
                                                    <LinkListItem 
                                                    loggedInUserIsAdmin={userIsAdmin}
                                                    as="Card"
                                                    key={ index }
                                                    item_key={index}
                                                    title={ item.page_title } 
                                                    shortcode={ item.shortcode } 
                                                    url={ item.url } 
                                                    desc={ item.desc } 
                                                    tags={ item.tags.split(",")} 
                                                    views={ item.views } 
                                                    createdAt={ item.created_at } 
                                                    is_public={ item.is_public }
                                                    user={ item.user } 
                                                    editMode={ editMode }
                                                    loggedInUser={user}
                                                    onDeleteClick={onDeleteClick} 
                                                    linksDeleteLoading={linksDeleteLoading}
                                                    linkDeleting={linkDeleting}
                                                    anyDeletePopupOpen={anyDeletePopupOpen}
                                                    handleDeletePopupOpen={handleAnyDeletePopupsOpen}
                                                    handlePrivacyToggle={handlePrivacyToggle}
                                                    handleViewsChange={handleViewsChange}
                                                    onTagClick={onTagClick}
                                                    baseUrl={baseUrl}
                                                    />
                                                );
                                            } else return null;
                                        }
                                    })
                                }
                            </Card.Group> 
                            <Divider fitted hidden />
                            <Divider fitted hidden />          
                        </Container>  
                    </Container>
                <Menu color={bottombar_color} borderless inverted icon fixed="bottom" size={Configuration.bottombar.size}>
                    <SortMenu open={sortBar_open} sortType={sortBy} changeSort={this.handleChangeSort} />
                    <Menu.Item name="sort" active={sortBar_open} onClick={this.handleSortBarToggle}>
                        { filterIcon }
                    </Menu.Item>
                    { editButton }
                    { refreshButton }
                </Menu>

            </Container>
        );
    }
}
