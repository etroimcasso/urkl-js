import React, { Component } from 'react';
import { Card, Label, Divider } from 'semantic-ui-react';
import ViewsLabel from '../bits/labels/ViewsLabel';
import CreatedAtLabel from '../bits/labels/CreatedAtLabel';
import ShortcodeTags from '../bits/labels/ShortcodeTags';
import PrivateShortcodeLabel from '../bits/labels/PrivateShortcodeLabel';
import UserLabel from '../bits/labels/UserLabel';
import ShareButton from '../bits/buttons/ShareButton';
import DeleteLinkButton from '../bits/buttons/DeleteLinkButton';
import ShortcodePrivacyToggle from '../bits/checkboxes/ShortcodePrivacyToggle';
import TagsButton from '../bits/buttons/TagsButton';
import Configuration from '../../config';

//Change the is_public setting via edit mode by:
// When privacy setting is changed, call a function in LinkList using a key from Listitem to change the is_public setting
// in the data object. Use this same function to dispatch an api call to update the entry in the database.

export default class LinkListItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: true,
            link_hover: false,
            privacy_change: false,
        }

        this.doNothing = this.doNothing.bind(this);
        this.handleLinkClick = this.handleLinkClick.bind(this);
        this.handleLinkEnterHover = this.handleLinkEnterHover.bind(this);
        this.handleLinkLeaveHover = this.handleLinkLeaveHover.bind(this);
        this.handlePrivacyToggle = this.handlePrivacyToggle.bind(this);
        this.handleTagClick = this.handleTagClick.bind(this);

    }
    componentDidMount() {

    }

    componentWillUpdate() {
        if (this.state.privacy_change) {
            this.setState()
        }
    }

    handlePrivacyToggle(is_public) {
        //Tell LinkList to edit the data object at item_key
        this.setState({
            is_public: is_public
        })
        this.props.handlePrivacyToggle(this.props.item_key, is_public);
    }

    handleTagClick(tag_index, do_delete) {
        if (!do_delete)
            this.props.onTagClick(this.props.item_key, tag_index, false);
    }

    doNothing() {

    }

    handleLinkClick() {
        this.props.handleViewsChange(this.props.item_key, this.props.views + 1);
        window.open('/' + this.props.shortcode, '_blank');
    }

    handleLinkEnterHover() {
        this.setState({
            link_hover: true
        })

    }

    handleLinkLeaveHover() {
        this.setState({
            link_hover: false
        })
    }

    render() {
        const { link_hover } = this.state;
        const { url, 
                shortcode,
                item_key,
                title, 
                createdAt, 
                desc, 
                user, 
                editMode, 
                loggedInUser, 
                visible,
                onDeleteClick,
                linksDeleteLoading,
                linkDeleting,
                anyDeletePopupOpen,
                handleDeletePopupOpen,
                loggedInUserIsAdmin,
                is_public,
                views,
                tags,
                baseUrl
            } = this.props;
        const handleTagClick = this.handleTagClick;

        const private_label = (is_public) ? null: (<PrivateShortcodeLabel shortcode={shortcode}/>);
        const userLabel = (user && user != 0) ? (<UserLabel user={user} />) : null;
        const private_card_color = (!is_public) ? Configuration.private_label_color : null;
        const shortcode_url = baseUrl + shortcode;

        var n_tags = tags
        if (n_tags == null) {
            n_tags = ""
        }

        const tags_element = (
            <TagsButton
                key={ item_key  }
                tags={ n_tags }
                onTagClick={ handleTagClick }
                shortcode = { shortcode }
            />
        );

        const loggedInUserIsItemUser = (user == loggedInUser && user != 0);
        const item_color = (editMode && (loggedInUserIsItemUser || loggedInUserIsAdmin)) ? Configuration.edit_mode_color: null; //(link_hover) ? Configuration.link_hover_color : null;

        //Determines whether it is appropriate to render this item




        /*
        const listItem =  (
            <Item>
                    <Item.Content>
                        <Item.Header as='a' href={ "/" + shortcode } target="_blank" title={ url } onClick={this.handleLinkClick}>{ title }</Item.Header>
                        <Item.Meta>
                            <Label.Group size={ Configuration.tag_size } style={{zIndex:-1, position: 'relative'}}>
                                { tags_element }
                            </Label.Group>
                        </Item.Meta>
                        <Item.Description>{ desc }</Item.Description>
                        <Item.Extra>
                        <Label.Group size={ Configuration.label_size } style={{zIndex:-1, position: 'relative'}}>
                                <ShortcodeUrlLabel shortcode={ shortcode } />
                                <ViewsLabel views={ views } />
                                <CreatedAtLabel createdAt={ createdAt } />
                                { userLabel }
                                { private_label }
                        </Label.Group>
                        </Item.Extra>

                    </Item.Content>
            </Item>
            );
        */
        const cardExtraContent = (editMode && (loggedInUserIsItemUser || loggedInUserIsAdmin )) ?
            (
            <Card.Content extra>
                { loggedInUserIsItemUser &&
                <span id="editModePrivacyToggle">
                    <ShortcodePrivacyToggle 
                        privacy={!is_public}
                        handleToggle={this.handlePrivacyToggle}
                        shortcode={shortcode}
                        updateDatabase={true}
                    />
                </span>
                } 
                <DeleteLinkButton name="deleteButton"
                    onClick={this.handleOtherClick}
                    item_key={item_key}
                    linksDeleteLoading={linksDeleteLoading}
                    linkDeleting={linkDeleting}
                    anyDeletePopupOpen={anyDeletePopupOpen}
                    floated='right'
                    onDeleteClick={onDeleteClick}
                    color={item_color}
                    handleDeletePopupOpen={handleDeletePopupOpen}
                />
            </Card.Content>)
        :  (
            <Card.Content extra name="cardExtras" onClick={this.handleClick}>
                <Label.Group as="span" id="cardInfoLabels" size={Configuration.label_size} >
                    <ViewsLabel views={ views } />
                    { tags_element }
                </Label.Group>
                <ShareButton 
                    floated='right'
                    shortcode_url={ shortcode_url }
                />
            </Card.Content>
            );

        const card_header = (
                <Card.Header className="linkInfoCardLink" onClick={this.handleLinkClick} title={url} >
                    { title }
                </Card.Header>
            );


        const userDisplay =  (userLabel) ? ( 
                        <Card.Content extra name="cardExtras" onClick={this.handleClick}>
                            { userLabel }
                        </Card.Content> ) : null;
        return ( 
                <Card onClick={this.doNothing} className="linkInfoCard" name="link_item" color={item_color} >
                    <Card.Content>
                        { private_label }
                        {card_header}
                        <Card.Meta>
                            <Divider className="infoCard itemInfoDivider" fitted hidden />
                            <CreatedAtLabel createdAt={ createdAt } />
                            <br />
                                <UserLabel user={user} />
                            <Divider className="infoCard itemInfoDivider" fitted hidden />
                        </Card.Meta>
                        <Card.Description>
                            { desc }
                        </Card.Description>
                    </Card.Content>
                    { cardExtraContent }
                </Card>
        );
    }
}