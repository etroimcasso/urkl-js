import React, { Component } from 'react';
import { Icon, Menu, Popup, Divider, Button } from 'semantic-ui-react';
import NewLinkModal from './NewLinkModal';
import LoginForm from './bits/forms/LoginForm';
import UserProfilePopupContent from './bits/popups/UserProfilePopupContent';
import CreateAccountForm from './bits/forms/CreateAccountForm';
import Configuration from '../config';


export default class TopBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            popup_profile: false,
            newLink_modal_open: false,
            popup_createAccount: false
        };

        this.handleCreateAccountClick = this.handleCreateAccountClick.bind(this);
        this.handleCreateAccountCancelClick = this.handleCreateAccountCancelClick.bind(this);
        this.handleAccountCreation = this.handleAccountCreation.bind(this);
        this.handleProfileButtonClick = this.handleProfileButtonClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNewLinkClick = this.handleNewLinkClick.bind(this);
    }

    componentDidMount() {
    }

    handleCreateAccountClick() {
        this.setState({
            popup_createAccount: true
        })
    }

    handleCreateAccountCancelClick() {
        this.setState({
            popup_createAccount: false
        })
    }

    handleAccountCreation(success) { 
        this.setState({
            popup_createAccount: !success,
            popup_profile: !success
        })
        this.props.onLoginFormSubmit();
    }

    handleProfileButtonClick() {
        if (!this.state.popup_profile) 
            this.setState({
                popup_createAccount: false
            })
        this.setState({
            popup_profile: !this.state.popup_profile,
            newLink_modal_open: false
        })
    }

    handleSubmit() {
        this.setState({
            popup_profile: false,
        })
        this.props.onLoginFormSubmit();
    }

    handleNewLinkClick() {
        this.setState({
            popup_profile: false,
            popup_createAccount: false
        });
        this.props.onClickNewLinkButton();
    }


    render() {
        const { newLink_modal_open, popup_createAccount, popup_profile } = this.state;
        const { contextRef, linksEditMode, user, onClickNewLinkButton, onSignout, userIsAdmin} = this.props;
        const color = (linksEditMode) ? Configuration.edit_mode_color : Configuration.topbar.menu_color;

        const popupForm = (!popup_createAccount) ?
        <LoginForm onCreateAccountClick={this.handleCreateAccountClick} key="_login_" handleSubmit={this.handleSubmit} /> :
        <CreateAccountForm onCreateAccount={this.handleAccountCreation} key="_createaccount_" onCreateAccountCancelClick={this.handleCreateAccountCancelClick} />

        const userProfilePopup = <UserProfilePopupContent key="_profile_" onSignout={onSignout} />;

        //const userProfileItemPopupHeaderText = (user == 0) ? "Sign in" : user;
        const profileButtonIcon = (user == 0);

        const newLink_colored = (Configuration.topbar.newLink_color != '');
        const profile_active = (Configuration.topbar.profile_color != '' || popup_profile);
        const badge_colored = (Configuration.topbar.badge_color != '');
        const badge_color = (badge_colored) ? Configuration.topbar.badge_color : null;
        const newLink_color = (newLink_colored) ? Configuration.topbar.newLink_color : null;
        const profile_color = (profile_active) ? Configuration.topbar.profile_color : null;

        const loggedInProfileButtonIcon = (userIsAdmin) ? (
                <Icon name='shield' />
            ):(
                <Icon name='user circle' />
            )

        return (
            <div>
                
                <Menu color={ color } inverted icon borderless fixed='top' size={ Configuration.topbar.size } >
                    <Popup 
                        hideOnScroll
                        trigger={(
                            <Menu.Item active={ badge_colored } color={ badge_color } header >
                                    <div id="badgeText" dangerouslySetInnerHTML={{__html: Configuration.text.badge_text }} />
                                </Menu.Item>
                            )} 
                        on='click'
                        header={Configuration.text.application_info}
                        content={<span><Icon name='copyright' />2018 Eric Tomasso</span>} />
                    <Menu.Item active={newLink_colored} color={ newLink_color } name='add link' onClick={this.handleNewLinkClick}
                        disabled={linksEditMode} >
                        <Icon name='plus' />
                    </Menu.Item>
                    <Popup open={popup_profile} id="signInProfilePopup" size={Configuration.profile_popup_size}
                        trigger={
                            <Menu.Item position='right' active={profile_active} color={profile_color} onClick={this.handleProfileButtonClick}>
                                { user != 0 &&
                                    <span className="profileButtonText">{loggedInProfileButtonIcon}{ user }</span>
                                }
                                { user == 0 &&
                                    <span className="profileButtonText">Sign in</span>
                                }
                            </Menu.Item>
                        }>
                        { user != 0 && // Logged in
                            <Popup.Content children={[userProfilePopup]}>
                            </Popup.Content>
                        }
                        { user == 0 && // Not Logged in
                            <Popup.Content children={[popupForm]}>
                            </Popup.Content>
                        }
                    </Popup>
                </Menu>
            </div>
        );
    }
}
