import React, { Component } from 'react';
import { Modal, Header, Button, Icon, Transition, Form, Input, Label, Checkbox, Container, Segment } from 'semantic-ui-react';
import CopyLinkToClipboard from './bits/buttons/CopyLinkToClipboard';
import ShortcodePrivacyToggle from './bits/checkboxes/ShortcodePrivacyToggle';
import Configuration from '../config';
import { isURL } from 'validator';
import normalizeUrl from 'normalize-url';
import axios from 'axios';

export default class NewLinkModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            shortcode_loading: false,
            title_loading: false,
            tags_loading: false,
            desc_loading: false,
            url: "",
            url_changed: false,
            title: "",
            desc: "",
            tags: [],
            shortcode: "",
            shortcode_public: true,
            desc_char_count: 0,
            title_error: false,
            tags_error: false,
            desc_error: false,
            confirm_open: false
        };

        this.getShortcode = this.getShortcode.bind(this);
        this.getUrlInformation = this.getUrlInformation.bind(this);
        this.handlePublicCheckboxClick = this.handlePublicCheckboxClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleUrlKeyPress = this.handleUrlKeyPress.bind(this);
        this.handleUrlInputChange = this.handleUrlInputChange.bind(this);
        this.submitNewUrl = this.submitNewUrl.bind(this);
        this.validateURL = this.validateURL.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.handleCancelClick = this.handleCancelClick.bind(this);
        this.handleConfirmSubmissionClick = this.handleConfirmSubmissionClick.bind(this);
        this.trimLongText = this.trimLongText.bind(this);
    }

    componentDidMount() {
        this.getShortcode();
    }

    componentDidUpdate() {

    }

    resetForm(refreshShortcode) {
        this.setState({
            confirm_open: false,
            url: "",
            title: "",
            tags: [],
            desc: "",
            desc_char_count: 0,
            shortcode_public: true
        });
        if (refreshShortcode)
            this.getShortcode();
    }

    handleCancelClick() {
        this.resetForm(false);
        this.props.onClickCancel();
    }

    handleConfirmSubmissionClick() {
        this.resetForm(true);
        this.props.onSubmitConfirm();
    }

    //Refreshes the shortcode 
    getShortcode() {
        this.setState({
            shortcode_loading: true
        });

        axios.get('/api/sc/gen')
            .then((response) => {
                this.setState({
                    shortcode: response.data.response,
                    shortcode_loading: false
                 })
            }
        );
    }

    validateURL(_url) {
        var n_url = "";
        try  {
            n_url = normalizeUrl(_url);
        } catch(error) {
                return false;
        }

        if (n_url.length > 0) {
            return isURL(normalizeUrl(_url), {
                require_tld: true,
                require_protocol: true,
                require_host: true,
                allow_underscores: true,
            });
        }
    }

    trimLongText(text, max_length, end_char) {
        return text.substr(0, max_length - end_char.length) + ((text.length >= max_length) ? end_char : "");
    }

    //Runs the title detection, etc sequence
    getUrlInformation() {

        /*
        Move this whole logic bit about what URL to use into the server, using /api/m/title_detect to 
            relay the results of either the server's internal title detection or the language processor's results.
            This way will allow me to use .env to determine the langproc settings and allow for cleaner fallback and
            simpler interface code
        */
        const n_url = this.state.url;
        console.log("url: " + this.state.url)

        if ( !this.state.title_loading && !this.state.tags_loading && !this.state.desc_loading ) {  // Input only allows this to be called if the URL is valid -- input validates itself
            this.setState({
                title_loading: true,
                title: "",
                url_changed: false,
                title_error: false,
                tags_error: false,
                desc_error: false,
                desc_loading: true,
                tags_loading: true,
                desc_char_count: 0,
                desc: "",
                tags: "",
            });


            //Title Detection
            axios.post('/api/m/title_detect', { url: n_url }).then((response) => {
                console.log('response: ' + response.data)
                if (response.data.response == false) {
                    this.setState({
                        title_loading: false,
                        title_error: true
                    })
                } else {
                    this.setState({
                        title: this.trimLongText(
                                            response.data.response, 
                                            Configuration.params.title_max_char_count, 
                                            Configuration.params.trimmed_text_suffix),
                        title_loading: false,
                   })
                }
            }).catch((error) => {
                this.setState({
                        title_loading: false,
                        title_error: true
                })
            })

            //Description Detection
            axios.post("/api/m/getLinkDescription", {url: n_url}).then((response) => {
                var desc_string = response.data.response;
                if (desc_string != false) {
                    if (desc_string.length > 0) {
                        desc_string = this.trimLongText(
                            desc_string, 
                            Configuration.params.link_desc_max_char_count, 
                            Configuration.params.trimmed_text_suffix)
                        }
                        this.setState({
                            desc: desc_string,
                            desc_char_count: desc_string.length,
                            desc_loading: false,
                        })
                    } else {
                        this.setState({
                            desc_loading: false
                        })
                    }
            }).catch((error) => {
                this.setState({
                    desc_loading: false,
                    desc_error: true
                })
            });

            //Article Keyword Detection
            axios.post("/api/m/detectArticleTags", { url: n_url }).then((response) => {
                    if (response.data.response) {
                        var n_tags = response.data.response;
                        var tags_string = "";
                        for (var i =0; i < n_tags.length; i++ ) {
                            var end_char = (i == (n_tags.length - 1)) ? "" : ", ";
                            tags_string = (tags_string + n_tags[i] + end_char);
                        }
                        this.setState({
                            tags: tags_string,
                            tags_loading: false,
                        })
                    } else {
                        this.setState({
                            tags_loading: false
                        })
                    }
            }).catch((error) => {
                    this.setState({
                        tags_loading: false,
                        tags_error: true
                    })
            });
        }
    }

    handlePublicCheckboxClick(is_public) {
        this.setState({
            shortcode_public: is_public
        })

    }

    handleChange(e, {name, value }) {
        this.setState({ [name]: value });
        if (name == "desc") {
            this.setState({
                desc_char_count: value.length,
                desc_error: false
            })
        } else if (name == "tags") {
            this.setState({
                tags_error: false
            })
        } else if (name == "title") {
            this.setState({
                title_error: false
            })
        } else if (name == "url") {
            this.getUrlInformation();
        }
    }

    
    handleUrlKeyPress(e) {
        if (e.key == 'Enter' || e.key == 'Meta')
            if (this.state.url_changed)
                this.getUrlInformation();
    }
    

    handleUrlInputChange(e) {
        this.setState({
            url: e.target.value,
            url_changed: true
        });
        /*
        if (this.validateURL(e.target.value)) {
            this.getUrlInformation();
        }
        */
    }

    submitNewUrl() {
        axios.post(("/api/sc/new"), {
            url: this.state.url,
            title: this.state.title,
            shortcode: this.state.shortcode,
            is_public: this.state.shortcode_public,
            tags: this.state.tags,
            user: this.props.user,
            image_id: this.state.shortcode,
            desc: this.state.desc
        }).then((response) => {
            this.setState({
                confirm_open: true,
            })
        }).catch((error) => {
            console.error("This shouldn't happen lol");
        });
    }

    render() {
        const { open, baseUrl } = this.props;
        const { 
            shortcode_loading,
            desc_char_count, 
            shortcode,
            shortcode_public,
            url,
            title,
            tags,
            desc,
            title_loading,
            tags_loading,
            desc_loading,
            url_changed,
            title_error,
            tags_error,
            desc_error,
            confirm_open,
        } = this.state;


        var shortcode_url = baseUrl + this.state.shortcode;

        const charCount = (desc_char_count >= Configuration.params.link_desc_max_char_count) ? 
            <Label color="orange" attached="bottom right">{desc_char_count}/{Configuration.params.link_desc_max_char_count}</Label>
            : <Label attached="bottom right">{desc_char_count}/{Configuration.params.link_desc_max_char_count}</Label>;

        const desc_loading_icon = (desc_loading) ? (<Icon name='refresh' loading={desc_loading} />) : null;

        const urlInfoIcon = (url_changed && 
                            this.validateURL(url) && 
                            !this.state.title_loading &&
                            !this.state.tags_loading &&
                            !this.state.desc_loading) ? 
            (
                <Icon name='quote left' color="green" circular link onClick={this.getUrlInformation} />
            ) :
            undefined;

        const submitButton = (!desc_loading && 
                              !tags_loading && 
                              !title_loading && 
                              this.validateURL(url) && 
                              title.length > 0 && 
                              shortcode.length > 0) ?
                <Button positive onClick={this.submitNewUrl}>Submit</Button> :
                <Button positive disabled>Submit</Button>;  

        const urlInput = (this.validateURL(url)) ? 
            (<Input fluid
                        name="url" 
                        label="URL" 
                        placeholder="" 
                        icon={ urlInfoIcon } 
                        value={url}
                        onInput={this.handleUrlInputChange}
                        onKeyUp={this.handleUrlKeyPress}
                    /> ):
            (<Input error fluid
                        name="url" 
                        label="URL" 
                        placeholder="" 
                        value={url}
                        onInput={this.handleUrlInputChange}
            />);

        const public_checkbox = (this.props.user != 0) ?
        (
            <Container textAlign="center">
                <ShortcodePrivacyToggle 
                    privacy={!shortcode_public}
                    handleToggle={this.handlePublicCheckboxClick}
                    shortcode={shortcode}
                    updateDatabase={false}
                />
            </Container>
            ) : null;


        const newLinkForm = (
            <Form size={Configuration.newLink_form_size}>
                <Form.Field required>
                    { urlInput }
                </Form.Field>
                <Form.Field>
                    <Input fluid
                    label="Title" 
                    name="title"
                    type="text"
                    placeholder=""
                    loading={title_loading } 
                    value={title}
                    onChange={this.handleChange}
                    maxLength={Configuration.params.title_max_char_count}
                    />
                </Form.Field>
                <Form.Field> 
                    <Input fluid
                        icon={ <Icon name='refresh' circular link onClick={this.getShortcode} /> }
                        loading={ shortcode_loading } 
                        name="shortcode" 
                        type="text" 
                        label="Shortcode" 
                        placeholder="" 
                        value={shortcode} 
                        readOnly />
                </Form.Field>
                <Form.Field>
                    <Input fluid 
                    placeholder="Tags" 
                    name="tags"
                    icon='tags'
                    iconPosition="left"
                    value={tags}
                    loading={ tags_loading }
                    onChange={this.handleChange}
                    />
                </Form.Field>
                { public_checkbox }
                <Form.Field>
                    <Segment basic padded>
                        <Label attached="top left">
                            {desc_loading_icon}
                            Description
                        </Label>
                        {charCount}
                        <Form.TextArea 
                            name="desc"
                            id="newLinkDescInput"
                            maxLength={Configuration.params.link_desc_max_char_count}
                            rows={5}
                            value={desc}
                            onChange={this.handleChange}
                        />
                    </Segment>
                </Form.Field>
            </Form>
        );


        return (
            <div>
                <Modal open={ open } id="newLinkModal" style={{ marginTop: '0px !important', marginLeft: 'auto', marginRight: 'auto' }} onOpen={this.handleNewLinkModalOpen}>
                    <Modal.Header  content="New Shortcode"></Modal.Header>
                    <Modal.Content>
                        { newLinkForm }
                    </Modal.Content>
                    <Modal.Actions>
                        <Button negative name="cancelButton" onClick={this.handleCancelClick}>Cancel</Button>
                        { submitButton }
                    </Modal.Actions>
                </Modal>
                <Transition visible={ confirm_open } animation='scale' duration={500}>
                    <Modal size="tiny" open={ confirm_open } style={{ marginTop: '0px !important', marginLeft: 'auto', marginRight: 'auto' }} >
                        <Modal.Header  content="Link Added"></Modal.Header>
                        <Modal.Content>
                            <Container textAlign='center'>
                                <CopyLinkToClipboard shortcode_url={shortcode_url}/>
                            </Container>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button positive name="confirmButton" onClick={this.handleConfirmSubmissionClick}>Close</Button>
                        </Modal.Actions>
                    </Modal>
                </Transition>
            </div>
        );
    }
}
