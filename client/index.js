import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import '../semantic/dist/semantic.min.css';
import './public/App.css';

ReactDOM.render(<App />, document.getElementById('root'));
