var express = require('express');
var router = express.Router();
var config = require('../../client/config.js');


/* Shortcode Controller */
var shortcodeController = require('../lib/controllers/shortcodes.js');
/* User Controller */
var userController = require('../lib/controllers/users.js');



// Create new URL shortcode
router.post('/new', function(req, res, next) {
    var url = req.body.url;
    var shortcode = req.body.shortcode;
    var title = req.body.title;
    var desc = req.body.desc;
    var is_public = req.body.is_public;
    var image_id = req.body.image_id; // For now
    var tags = req.body.tags;
    var user = req.body.user;
    if( req.session.userId) {
        userController.getUserById(req.session.userId, function (err, user) {
            if (err) throw err;
            shortcodeController.createNewEntry(
                shortcode, url, title, desc, image_id, is_public, user.username, tags, function(err, success) {
                    res.json({
                        response: success
                    });
                });
        });
    } else {
        shortcodeController.createNewEntry(
            shortcode, url, title, desc, image_id, is_public, user, tags,
            function(err, success) {
                res.json({
                    response: success
                });
        });
    }
});

//Remove Shortcode
router.post('/rem', function(req, res, next) {
    var shortcode = req.body.shortcode;
    shortcodeController.deleteEntry(shortcode, function(err, result) {
        res.json({
            response: result
        })
    })
    
});

/* GET routes */

//Generate new shortcode
router.get('/gen', function (req, res, next) {
    shortcodeController.getNewShortcode(function(err, result) {
        if (!err)
            res.json({ 
                response: result 
       });
    });

});



//Get image_id
//Takes URL and Shortcode
router.post('/image_id', function (req, res, next) {
    res.json({ 
        response: 'image_id'
    });

});

//Get ALL shortcodes
router.get('/lists/all', function (req, res, next) {
    shortcodeController.getAll( function(items) {
        res.json({ 
            response: items.reverse()
        });
    });
});


//Returns the required shortcode length
router.get('/length', function (req, res, next) {
    res.json({ 
        response: shortcodeController.getLength()
    });

});

router.get('/possible', function (req, res, next) {
    res.json({
        response: shortcodeController.getPossible()
    });
});

//Returns the number of remaining shortcodes
router.get('/remain', function (req, res, next) {
    shortcodeController.getRemaining(function(count) {
        res.json({ 
            response: count
        });
    });
});

//Gets the number of shortcodes in database
router.get('/count', function (req, res, next) {
    shortcodeController.getCount(function(count) {
        res.json({ 
            response: count
        });
    });
});

//Returns information for the specified shortcode
// TODO: Change this to a POST and get the shortcode from the request body
router.get('/info/:shortcode', function (req, res, next){
    shortcodeController.getShortcodeObject(req.params.shortcode, function(item) {
        res.json({ 
            response: item
        });
    });


});

router.post('/updatePrivacySetting', function (req, res, next) {
    var shortcode = req.body.shortcode;
    shortcodeController.toggleEntryPrivacy(shortcode, function(err, result) {
        if (err) res.json({
            response: false,
            error: err
        })
        else {
            res.json({
                response: true
            })
        }
    })
})



module.exports = router;