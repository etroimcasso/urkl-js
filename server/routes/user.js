var express = require('express');
var router = express.Router();

/* User Controller */
var userController = require('../lib/controllers/users.js');

//Get user info
router.post('/info', function(req, res, next) {
    userController.getUserByUsername(req.body.username, function(err, user) {
        res.json({
            response: user
        })
    })
});

/*
router.get('/lists/all', function(req, res, next){
    userController.getAllUsers(function(users) {
        res.json({
            response: users
        })
    })
});
*/

router.post('/set/timezone', function (req, res, next) {
    var timezone = req.body.timezone;

    userController.getUserById(req.session.userId, function(err, user) {
        if (err) throw err;
        userController.updateUser(user.username, 'timezone', timezone, function (err, result) {
            if (err) throw err
            else res.json({
                response: result
            })
        })
    });


})

router.post('/new', function(req, res, next) {
    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;


    //Returns TRUE if successful
    userController.insertUser(username, password, email, function(err, user) {
        //If result is false
        console.log("ERROR: " + err)
        if (err) res.json ({
            response: false,
            error: err
        });

        if (user == false) {
            res.json ({
                response: false,
                error: err
            });
        } else {
            //req.session.userId = result;
            req.session.userId = user._id;

            res.json({
                response: true
            })
        }   
    })
});

//Returns number of users
router.get('/count', function (req, res, next) {
    userController.getUserCount(function (users) {
        res.json({
            response: users
        })
    })
});


//Logs user in
router.post('/login', function (req, res, next) {
    userController.authenticateUser(req.body.username, req.body.password, function (err, user) {
        if (err)  { 
            res.json({
                response: false
            })
        } else {
            req.session.userId = user._id;
            
            userController.updateUser(user.username, 'last_visited', null, function(err, result) {
                if (err) throw err
                res.json({
                    response: true
                })
            })
        }
    })
});

//returns 0 to indicate no user or error, fallback to no user
router.get('/getSessionUserData', function (req, res, next){
    if (req.session) {
        userController.getUserById(req.session.userId, function(err, user) {
            if (err) {
                console.log("Error in getSessionUserData");
                res.json({
                    response: false
                })
            };
            if (user) {
                res.json({
                    response: user
                });
            } else {
                res.json({
                    response: false
                })
            }
        })
    } else {
        console.log("No user") 
        res.json({
            response: false
        });
    }  

})

router.get('/logout', function (req, res, next) {
    if (req.session) {
        req.session.destroy(function (err) {
            if (err) return next(err);
            else { 
                res.json({
                    response: true
                });
            }
        })
    }
})

// Returns true if the user exists
router.post('/doesUserExist', function (req, res, next) {
    userController.getUserByUsername(req.body.user, function(err, user) {
        res.json({
            response: (user != null && err == null) ? true : false
        })
    })

})

// Get specific user data
router.post('/getUserData', function (req, res, next) {
    userController.getUserByUsername(req.body.username, function (err, user) {
        res.json({
            response: (user != null) ? user : null
        })
    })
})

module.exports = router;

