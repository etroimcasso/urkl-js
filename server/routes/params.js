require('dotenv').config();
var express = require('express');
var router = express.Router();
var config = require('../../client/config.js');

router.get('/all', function(req, res, next) {
    res.json({
        max_title_length: config.params.title_max_char_count,
        max_desc_length: config.params.link_desc_max_char_count,
        min_username_length: config.params.min_username_length,
        max_username_length: config.params.max_username_length,
        min_password_length: config.params.min_password_length,
        max_password_length: config.params.max_password_length,
        trimmed_text_suffix: config.params.trimmed_text_suffix
    })
})
/*
//Returns the allowed Title length
router.get('/max_title_length', function (req, res, next) {
    res.json({
        response: config.params.title_max_char_count
    })
})

//Returns the maximum description length
router.get('/max_desc_length', function (req, res, next) {
    res.json({
        response: config.params.title_max_char_count
    })
})

//Returns minimum username length
router.get('/min_username_length', function(req, res, next) {
    res.json({
        response: config.params.min_username_length
    })
})

//Returns maximum username length
router.get('/max_username_length', function(req, res, next) {
    res.json({
        response: config.params.max_username_length
    })
})

//Returns minimum password length
router.get('/min_password_length', function(req, res, next) {
    res.json({
        response: config.params.min_password_length
    })
})

//Returns maximum password length
router.get('/max_password_length', function(req, res, next) {
    res.json({
        response: config.params.max_password_length
    })
})

//Returns trimmed_text_suffix
router.get('/trimmed_text_suffix', function(req, res, next) {
    res.json({
        response: config.params.trimmed_text_suffix
    })
})
*/



module.exports = router;