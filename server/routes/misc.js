require('dotenv').config();
var express = require('express');
var router = express.Router();
var axios = require('axios');
var getTitleAtUrl = require('get-title-at-url');
var config = require('../../client/config.js');

const lang_url = process.env.LANG_HOST + ":" + process.env.LANG_PORT


//Detect page title
router.post('/title_detect', function (req, res, next) {
    if (process.env.LANG_PROC === "true") {
        axios.post(lang_url + "/detectArticleTitle", { 
            url: req.body.url
        }).then((response) => {
            if (response.data.response.length == 0) {
                console.log("Falling back to native title detection");
                nativeTitleDetectionFallback(req.body.url, function(title) {
                    res.json({
                        response: title
                    })
                })
            } else {
                res.json({
                    response: response.data.response
                });
            }
        }).catch((error) => {
            console.log("Falling back to native title detection");
            nativeTitleDetectionFallback(req.body.url, function(title) {
                res.json({
                    response: title
                })
            })
        });
    } else {
        console.log("Falling back to native title detection");
        nativeTitleDetectionFallback(req.body.url, function(title) {
            res.json({
                response: title
            })
        })
    }
});

function nativeTitleDetectionFallback(url, callback) {
    getTitleAtUrl(url, function(title) {
        if (title) {
            if (title.length == 0) {
                return callback(false);
            }
            return callback(title);
        } else {
            return callback(false);
        }
    })
}

router.post('/getLinkDescription', function(req, res, next) {
    if (process.env.LANG_PROC === "true") {
        axios.post(lang_url + "/getArticleBodyText", {url: req.body.url}).then((response) => {
            res.json({
                response: response.data.response
            })
        }).catch((error) => {
            res.json({
                response: false
            })
        });
    } else {
        res.json({
            response: false
        })
    }
});

router.post('/detectArticleTags', function(req, res, next) {
    if (process.env.LANG_PROC === "true") {
        axios.post(lang_url + "/detectArticleTags", {url: req.body.url }).then((response) => {
            res.json({
                response: response.data.response
            })
        }).catch((error) => {
            res.json({
                response: false
            })
        });
    } else {
        res.json({
            response: false
        })
    }
});

module.exports = router;