require('dotenv').config();

var express = require('express');
var router = express.Router();
var path = require('path');
const config = require('../../client/config.js');
var mongoose = require('mongoose');
var mergeJSON = require('merge-json');
var timezoneList = require('timezone-list').getTimezones();


var shortcodeController = require('../lib/controllers/shortcodes.js'); // Shortcode Controller 
var userController = require('../lib/controllers/users.js');


//This is no longer needed as all 404s will redirect to root, and any non-existant 
//shortcode redirects to root as well
//const shortcode_regex = '(([A-Za-z0-9]){' +  process.env.SHORTCODE_LENGTH + '})';


/* GET home page. */

router.get('/', function(req, res, next) {
    res.render('index', {
        page_title: config.text.page_title,
        titlebar_color: config.titlebar_color
    });
    /*
    //Retrieve user info based on Session ID using getUserById(sessionId, callback)
    shortcodeController.getAll(function(items) {
        var default_data = { 
            data: {
                baseUrl: process.env.BASE_URL,
                shortcodes: 
                    items.reverse(),
                timezones: timezoneList.sort()
            },
            frontend: {
                page_title: config.frontend.text.page_title,
                application_text: config.frontend.text.application_text,
                badge_text: config.frontend.menubar.badge.text,
                badge_info: config.frontend.menubar.badge.info_text,
                application_info: config.frontend.text.application_info,
                menubar: {
                    size: config.frontend.menubar.size,
                    location: config.frontend.menubar.location,
                    type: config.frontend.menubar.type
                },
            }
        }
        var login_data = 
            { 
                logged_in: false 
            };
        if (!req.session.userId) {
            var json_data = mergeJSON.merge(default_data, login_data);
            res.render('index', json_data);
        } else { 

            userController.getUserById(req.session.userId, function(err, user) {
                if (err) throw err;
                login_data = 
                {
                    logged_in: true,
                    userdata: {
                        username: user.username,
                        timezone: user.timezone,
                        avatar_id: (user.avatar_id == null) ? "image.png" : user.avatar_id,
                        join_date: user.join_date,
                        last_visited: user.last_visited,
                    }
                }; 

                var json_data = mergeJSON.merge(default_data, login_data);
                res.render('index', json_data);

            });
        }


    });
    */
});


router.get('/robots.txt', function (req, res, next) {
    if (process.env.NO_ROBOTS) {
        res.sendFile(path.join(__dirname, '../lib/public/robots.txt'), 
            function(err) {
                if (err) throw err;
        });
    }
});

/* Shortcode Redirection */
router.get("/:shortcode", function (req, res, next) {

    var shortcode = req.params.shortcode;


    //Retrieve shortcode object
    //Check if shortcode object is public
    //if public, redirect
    //If not, check if user is logged in and owns shortcode object
    //Otherwise, redirect to root ('/')

    //This catches the favicon.ico route error
    if (shortcode != "favicon.ico") {
        shortcodeController.getShortcodeObject(shortcode, function(err, shortcodeObj) {
            if (err == "NOSC" || !shortcodeObj) res.redirect('/');
            else {
                if (shortcodeObj.is_public) {
                    redirectToShortcodeUrl(res, shortcode, shortcodeObj.url);
                } else if (req.session.userId) {
                    userController.getUserById(req.session.userId, function (err, user) {
                        var username = user.username;
                        if (username === shortcodeObj.user) {
                            redirectToShortcodeUrl(res, shortcode, shortcodeObj.url);
                        } 
                        else res.redirect('/');
                    })
                } else {
                    res.redirect('/');
                }

            }
        })
    }
});

function redirectToShortcodeUrl(res, shortcode, url) {
    shortcodeController.increaseViews(shortcode);
    res.redirect(url);
}


module.exports = router;
