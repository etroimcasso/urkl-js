var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    shortcode: { type: String, required: true, unique: true },
    url: { type: String, required: true, index: true },
    page_title: { type: String, required: true },
    desc: { type: String },
    image_id: { type: String, required: true },
    o_image_id: { type: Boolean, required: true },
    is_public: { type: Boolean, required: true },
    user: { type: String, required: true, index: true },
    rating: { type: Number, required: true, index: true, min: 0 },
    total_ratings: { type: Number, required: true, min: 0 },
    views: { type: Number, required: true, min: 0 },
    created_at: { type: Date, required: true, index: true },
    tags: { type: String, required: false, index: true }
});

module.exports = mongoose.model('Shortcode', schema);