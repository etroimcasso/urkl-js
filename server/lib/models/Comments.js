var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    shortcode: { type: String, required: true, unique: true },
    username: { type: String, required: true, index: true },
    created_at: { type: Date, required: true, index: true },
    comment: {type: String, required: true, index: false}
});

schema.index({ shortcode: 1, username: 1, created_at: 1}, { unique: true });

module.exports = mongoose.model('ShortcodeComments', schema);