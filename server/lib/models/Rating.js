var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username: {
        type: String,
        required: true,
        trim: true,
        index: true
    },
    shortcode: { type: String, required: true, unique: false },

});

schema.index({ username: 1,shortcode: 1 }, { unique: true });


module.exports = mongoose.model('Rating', schema);