require('dotenv').config();

const randomInt = require('random-int');
const normalizeUrl = require('normalize-url');


//Mongoose Stuff
var mongoose = require('mongoose');
var Shortcode = require('../models/Shortcode.js'); //Shortcode model

const __MONGO_URI__ = 'mongodb://' + 
                process.env.MONGO_USER + ':' +
                process.env.MONGO_PASS + '@' +
                process.env.MONGO_HOST + ':' +
                process.env.MONGO_PORT + '/' +
                process.env.MONGO_DB;

                
mongoose.connect(__MONGO_URI__);




/* Load values from Settings file */
const __length__ = process.env.SHORTCODE_LENGTH;

/*
(Total number of characters available minus _ and - for first character)
 - (total number of characters available)^(shortcode length - 1)
*/
const __total_possible_characters__ = 62;
const __total_possible__ = Math.pow(__total_possible_characters__, __length__);


//Generates the actual shortcode
/*
    CHARACTER CODES:
    65 - 90 : Uppercase
    97 - 122: lowercase
*/
function generateShortcode () {
    var shortcode = new Array(__length__);

    for (i = 0; i < __length__; i++) {
        //Prevents first character from being underscore or dash 
        switch (randomInt(0,8)) {
            case 0:
            case 1:
            case 2:
                //uppercase
                var n_char =  String.fromCharCode(randomInt(65,90));
                break;
            case 3:
            case 4:
            case 5:
                //lowercase
                var n_char = String.fromCharCode(randomInt(97,122));
                break;
            case 6:
            case 7:
            case 8:
                //number
                var n_char = randomInt(0,9);
                break;
        }
        //Add the generated character to the shortcode array
        shortcode[i] = n_char;
    }

    return shortcode.join("");
}

//Returns true or false
function isUrlDuplicate(url, callback) {
    url = normalizeUrl(url);

    Shortcode.count({url: url}).exec(function(err, count) {
        if (err) throw err
        return callback(count > 1);
    });
}

//Returns true or false depending on if specified shortcode already exists
function isShortcodeUnique(shortcode, callback) {
    Shortcode.count({ shortcode: shortcode }).exec(function(err, count) {
        if (err) throw err;
        return callback(null, count == 0);
    });
}

function getShortcodeCount(callback) {
    Shortcode.count({}).exec(function(err, count) {
        if (err) throw err;

        return callback(count);
    });
}


var publicFunctions = {

    //Returns required length of shortcodes

    getLength: function() {
        return __length__;
    },

    //Returns a new unique shortcode
    // Will generate a new code until it is unique
    //STILL DOES NOT ACTUALLY CHECK FOR UNIQUENESS
    getNewShortcode: function (callback) {
        var shortcode = "";
        //var sc_unique = false;
       // do { 
            shortcode = generateShortcode();
            isShortcodeUnique(shortcode, function(err, unique){
                //console.log("err: " + err);
                //console.log("unique: " + unique);
                if (unique) { 
                    callback(null, shortcode)
                }
            }) 
        //} while (sc_unique == false)
        

    },

    //Creates new entry
    createNewEntry: function(shortcode, url, title, desc, image_id, is_public, user, tags, callback) {
        //console.log("Tags:" + tags)
        isUrlDuplicate(url, function(is_duplicate) {
            Shortcode.create({
                shortcode: shortcode,
                url: normalizeUrl(url),
                page_title: title,
                desc: desc,
                image_id: image_id,
                o_image_id: !is_duplicate,
                is_public: is_public,
                user: user,
                created_at: Date.now(),
                rating: 0,
                total_ratings: 0,
                views: 0,
                tags: tags
            }, function(err, item) {
                if (err) {
                    console.log("ERROR: " + err);
                    return callback(err, false);
                }
                return callback(null, true);
            });
        });
        

    },

    /* getImageID Should
    Basically, check if the URL is duplicate. If it is, grab the oldest shortcode with this URL
    and create a new screenshot of the website using that shortcode as the image_id. The shortcode
    of the oldest duplicate URL will be the image_id of the new URL.
        If the URL isn't duplicate, the image_id is the same as the shortcode input, and the 
        screenshot command is called with this shortcode.

    This way, any new URL has its own shortcode as its image_id, and all duplicates of this URL
    will point to the same image_id. Image will be updated each time the URL is duplicated in the 
    database.

    1. Take a shortcode and a URL as input
    2. Check if URL is duplicate.
        *is Duplicate: 
            1. Retrieve shortcode of oldest copy of this URL
            2. Call updateScreenshot( oldest shortcode)
                2a Maybe easier to do using an o_imgid flag?
                    - Just set flag to true, when URL duplicate, search for the URL with the true flag set
            3. Return oldest shortcode
        *is not duplicate:
            1. Use shortcode input to call screenshotting command.
            2. Set original imageID flag with shortcode input
            3. Return shortcode input
    */

    getImageId: function(shortcode, url) {
        if (isUrlDuplicate(url)) {
            // o_imgId getOriginalImageId(url);

        } else {

        }

    },

    getShortcodeObject: function(shortcode, callback) {
        Shortcode.findOne({ 'shortcode' : shortcode }, function(err, item) {
            if (err) throw err;
            return callback(null, item);
        });
    },

    //Retreves URL for specified shortcode
    getUrl: function(shortcode, callback) {
        Shortcode.findOne({ 'shortcode' : shortcode }, 'url', function(err, item) {
            if (err) return callback(err);
            if (item) return callback(null, item.url);
            else {
                return callback("NOSC", null);
            }
        });
    },

    //Returns all shortcodes
    getAll: function(callback) {
        Shortcode.find({}).exec(function(err, items) {
            if (err) console.log("ERROR: " + err);
            return callback(items);
        });
    },

    getPossible: function() {
        return __total_possible__;
    },

    //Returns number of remaining shortcodes
    getRemaining: function(callback) {
        getShortcodeCount(function(count) {
            return callback(__total_possible__ - count);

        })
    },

    //Returns number of shortcodes in database
    getCount: function(callback) {
        getShortcodeCount(function(count) {
            return callback(count);

        })
    },

    getShortcodesForUser: function(username, callback) {
        Shortcode.find({user: username}, function(err, shortcodes){
            if (err) return err;
            return callback(null, shortcodes);
        })
    },

    //Increase views for specified shortcode by 1
    increaseViews: function(shortcode) {
        Shortcode.findOneAndUpdate({ shortcode: shortcode }, { $inc: { views: 1 }}, function(err, item) {
            item.save();
        });
    },

    //Removes entry
    deleteEntry: function(shortcode, callback) {
        Shortcode.remove({shortcode: shortcode}, function(err, result) {
            if (err) return (err, false);
            return callback(null, true)
        })
    },

    toggleEntryPrivacy: function(shortcode, callback) {
        Shortcode.findOne({shortcode: shortcode}, function(err, item) {
            Shortcode.findOneAndUpdate({shortcode: shortcode}, { $set: { is_public: !item.is_public }}, function(err, item) {
                if (err) return callback(err)
                item.save();
                return callback(null, true)
            })
        })
    }
}



module.exports = publicFunctions;