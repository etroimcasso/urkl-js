require('dotenv').config();

var Enum = require('enum');
var bcrypt = require('bcrypt');

/*AVATAR_ID: When a user account is created, a new folder with their _id is
    created for storing their avatars and other images. Avatars will be saved with 
    the name "av_u.(image extension)"
*/


//Mongoose Stuff
var mongoose = require('mongoose');
var User = require('../models/User.js'); //Shortcode model

const __MONGO_URI__ = 'mongodb://' + 
                process.env.MONGO_USER + ':' +
                process.env.MONGO_PASS + '@' +
                process.env.MONGO_HOST + ':' +
                process.env.MONGO_PORT + '/' +
                process.env.MONGO_DB;

                
mongoose.connect(__MONGO_URI__);



/* 
Flow of user stuff:
•Create new user
•When user logs in, confirm their timezone / location
*/

//USERNAME is ID

//Load mongo configuration
const __mongo_url__ = 'mongodb://' 
    + process.env.MONGO_HOST + ":" 
    + process.env.MONGO_PORT;

const __mongo_db__  = process.env.MONGO_DB;
const __mongo_user__ = process.env.MONGO_USER;
const __mongo_pass__ = process.env.MONGO_PASS;

//Add Sessions System


function isUserUnique(username, callback) {
    User.count({username: username}).exec(function(err, count) {
        if (err) throw err;
        return callback(count == 0); //Returns true if no matches found
    })

};

function isEmailUnique(email, callback) {
    User.count({email: email}).exec(function(err, count) {
        if (err) throw err;
        return callback(count == 0); //Returns true if no matches found
    })
};



var publicExports = {

    //Update option enumerator
    //Add more options later
    userUpdateOptions: Enum [(
        'email',
        'username',
        'f_name',
        'l_name',
        'location',
        'timezone',
        'session_id',
        'avatar_id',
        'last_visited'
    )],

    /* Creating a user requires:
        •Username
        •password (will be hashed & salted before storage in database)
        •email address
    */
    insertUser: function(username,password,email,callback) {
        username = username.toLowerCase();
        //Then check if Username is unique
        //Then check if email is unique
        //When all these conditions are met, add user to database 
        bcrypt.hash(password, 10, function (err, hashed_pass) {    
            if (err) return next(err);
            isUserUnique(username, function(user_unique) {
                if (user_unique) {
                    isEmailUnique(email, function(email_unique) {
                        if (email_unique) {
                            User.create({
                                username: username,
                                email: email,
                                timezone: 'etc/UTC',
                                join_date: Date.now(),
                                last_visited: Date.now(),
                                password: hashed_pass,
                                admin: false
                            }, function (err, user) {
                                if (err) throw err
                                return callback(null, user);
                            });
                        } else {
                            //RETURN THAT EMAIL IS NOT UNIQUE
                            console.log("Email not unique");
                            return callback(null, false);
                        }
                    });
                } else {
                    //RETURN THAT USERNAME IS NOT UNIQUE
                    console.log("Password not unique");
                    return callback(null, false);
                }
            });
        });
           
    },

    deleteUser: function(username) {

    },

    updateUser: function(username, userOptions, updateParams, callback) {
        switch (userOptions) {
            case 'email':
                break;
            case 'username':
                break;
            case 'f_name':
                break;
            case 'l_name':
                break;
            case 'location':
                break;
            case 'timezone':
                User.findOneAndUpdate({ username: username }, { timezone: updateParams }, function(err, item) {
                    if (err) throw err;
                    item.save();
                    return callback(null, true)
                });
                break;
            case 'session_id':
                break;
            case 'avatar_id':
                break;
            case 'last_visited':
                User.findOneAndUpdate({ username: username }, { last_visited: Date.now() }, function(err, item) {
                    if (err) throw err;
                    item.save();
                    return callback(null, true)
                });
                break;
            default:
                break;
        }

    },

    getAllUsers: function(callback) {
        User.find({},'username email timezone location join_date last_visited admin').exec(function(err, items) {
            if (err) throw err
            return callback(items);
        });
    },

    getUserCount: function(callback) {
        User.count({}).exec(function(err, count) {
            if (err) throw err;
            return callback(count);
        });
    },

    getUserByUsername: function(username, callback) {
       User.findOne({ 'username' : username },'username email timezone location join_date last_visited admin', function(err, item) {
            if (err) throw err;
            return callback(null, item);
        });
    },

    getUserById: function(userId, callback) {
       User.findOne({ '_id' : userId },'username email timezone location join_date last_visited admin', function(err, item) {
            if (err) throw err;
            return callback(null, item);
        });
    },

    //Compares the hash of 'password' with the password hash associatated with 'username'
    authenticateUser(username, password, callback) {
        User.authenticate(username, password, function(err, auth) {
            if (err) return callback(err);
            return callback(null, auth);
        });
    },
}

module.exports = publicExports;