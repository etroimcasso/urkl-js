require('dotenv').config();
const ShortcodeController = require('./shortcodes');

const LENGTH = process.env.SHORTCODE_LENGTH;
const POSSIBLE = 62 ** LENGTH;


test('Shortcode length has been assigned in .env configuration file', () => {
    expect(LENGTH).not.toBeNull();
});

test('Newly generated shortcodes match length value in env', () => {
    ShortcodeController.getNewShortcode(function(err, shortcode) {
        expect(shortcode.length).toBe(LENGTH);
    });
});

test('Newly generated shortcode contains only A-Z,a-z,0-9', () => {
    ShortcodeController.getNewShortcode(function(err, shortcode) {
        //Regex to match [A-Za-z0-9]{LENGTH}
    })
}) 

test('Reported possible number of shortcodes should equal 62^length of shortcode', () => {
    var possible = ShortcodeController.getPossible();
    expect(possible).toBe(POSSIBLE);
})