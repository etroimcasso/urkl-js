require('dotenv').config();

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
const MongoStore = require('connect-mongo')(session);
var mongoose = require('mongoose');
var forceSsl = require('express-force-ssl');
const config = require('../client/config.js');




const __MONGO_URI__ = 'mongodb://' + 
                process.env.MONGO_USER + ':' +
                process.env.MONGO_PASS + '@' +
                process.env.MONGO_HOST + ':' +
                process.env.MONGO_PORT + '/' +
                process.env.MONGO_DB;

                
mongoose.connect(__MONGO_URI__);




var api_user = require('./routes/user');
var api_misc = require('./routes/misc');
var api_shortcode = require('./routes/shortcode');
var api_params = require('./routes/params');
var index = require('./routes/index');

var app = express();

app.use(forceSsl);


//Add Enum capabilities 
require('enum').register();


// view engine setup

app.set('views', path.join(__dirname, '../client'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



//Sessions
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: mongoose.connection
  })
}))


app.use('/api/u', api_user);
app.use('/api/sc', api_shortcode);
app.use('/api/m', api_misc);
app.use('/api/p', api_params);

app.use('/', index);


//Client Public folder
app.use('/src',express.static(path.join(__dirname, '../client/public')));
app.use('/sta',express.static(path.join(__dirname, '../client/public/sta')));

//Images path
app.use('/img',express.static(path.join(__dirname, 'storage/images')));

//Template things
//app.locals.template_label_size = config.frontend.label_size;



// catch 404 and redirect to homepage
app.use(function(req, res, next) {
  res.redirect('/');
  
  //var err = new Error('Not Found');
  //err.status = 404;

  //next(err);
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {
    error: {
        status: err.status,
        message: err.message
    }});
});

module.exports = app;
