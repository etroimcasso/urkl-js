//Configuration file for domain / site detection

module.exports = {
    sites:{
        {
            name: 'Google',
            type: 'search engine',
            urls: [
                'google.com',
                'google.co.uk'
            ],
            color: 'blue',
        },
        {
            name: 'Youtube',
            type: 'videos',
            urls: [
                'youtube.com'
            ],
            color: 'red'
        },
        {
            name: 'Facebook',
            type: 'social media',
            urls: [
                'facebook.com'
            ],
            color: 'blue'
        },
        {
            name: 'Reddit',
            type: 'social media',
            urls: [
                'reddit.com'
            ],
            color: 'orange'
        },
        {
            name: 'Tumblr',
            type: 'blog',
            urls: [
                'tumblr.com'
            ],
            color: 'blue'
        },
        {
            name: 'Gmail',
            type: 'email',
            urls: [
                'mail.gmail.com',
                'gmail.com',
            ],
            color: 'blue',
        },
        {
            name: 'Bing',
            type: 'search engine',
            urls: [
                'bing.com'
            ],
            color: 'teal',
        },
        {
            name: 'Amazon',
            type: 'store',
            urls: [
                'amazon.com',
                'amazon.co.uk',
                'amazon.ca',
            ],
            color: 'yellow',
        },
        {
            name: 'Newegg',
            type: 'store',
            urls: [
                'newegg.com'
            ],
            color: 'yellow',
        },
    }
